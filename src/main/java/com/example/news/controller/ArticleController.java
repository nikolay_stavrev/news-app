package com.example.news.controller;

import com.example.news.model.Article;
import com.example.news.repository.ArticleRepository;
import com.example.news.repository.ArticleSearchRepository;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManagerFactory;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@RestController
@RequestMapping("/articles")
public class ArticleController {

    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private ArticleSearchRepository articleSearchRepository;


    @RequestMapping(produces = MediaType.APPLICATION_JSON)
    public List<Article> findAll(){

        Iterable<Article> iterable = articleRepository.findAll();
        Iterator<Article> iterator = iterable.iterator();

        List<Article> result = new ArrayList<>();

        while(iterator.hasNext()){
            result.add(iterator.next());
        }

        return result;
    }

    @RequestMapping(value = "/search", produces = MediaType.APPLICATION_JSON)
    public List<Article> searchByKeyword(@RequestParam("term") String term){
       return articleSearchRepository.searchByKeyword(term);
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON)
    public Article create(@RequestBody Article article){
        return articleRepository.save(article);
    }

}
