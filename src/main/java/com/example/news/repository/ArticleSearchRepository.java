package com.example.news.repository;


import com.example.news.model.Article;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class ArticleSearchRepository {

    @Autowired
    private SessionFactory sessionFactory;

    public List<Article> searchByKeyword(String keyword){
        Session currentSession = sessionFactory.getCurrentSession();
        FullTextSession fullTextSession = Search.getFullTextSession(currentSession);

        QueryBuilder qb = fullTextSession.getSearchFactory()
                .buildQueryBuilder().forEntity( Article.class ).get();

        org.apache.lucene.search.Query query = qb
                .keyword()
                .onFields("title", "description")
                .matching(keyword)
                .createQuery();

        Query hibQuery =
                fullTextSession.createFullTextQuery(query, Article.class);

        // execute search
        return hibQuery.list();
    }

}
