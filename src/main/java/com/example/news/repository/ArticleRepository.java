package com.example.news.repository;

import com.example.news.model.Article;
import org.springframework.data.repository.CrudRepository;

public interface ArticleRepository extends CrudRepository<Article,Long>{
}
